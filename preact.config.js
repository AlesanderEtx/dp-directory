export default (config, env, helpers) => {
  console.log("process", process.env.CI_PROJECT_NAME);
  if (process.env.CI_PROJECT_NAME) {
    config.output.publicPath = `/${process.env.CI_PROJECT_NAME}/`;
    // // use the public path in your app as 'process.env.PUBLIC_PATH'
    config.plugins.push(
      new helpers.webpack.DefinePlugin({
        "process.env.PUBLIC_PATH": JSON.stringify(
          config.output.publicPath || "/"
        ),
      })
    );
  }
};
